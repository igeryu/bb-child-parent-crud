package com.magipixel.bountyboard.childparent;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Map;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@Tag("integration")
class ChildParentIT {

    private static int idCount = 0;

    @BeforeAll
    static void resetIdCount() {
        ChildParentIT.idCount = 0;
    }

    @Nested
    @DisplayName("Security")
    @AutoConfigureMockMvc
    @SpringBootTest
    class Security {

        private final Random random = new Random();
        @Autowired
        private MockMvc mockMvc;

        @BeforeEach
        void addChildParent() throws Exception {
            final long childId = random.nextLong();
            mockMvc.perform(post("/childParents")
                    .with(user("username")
                            .roles("PARENT"))
                    .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"childId\": " + childId + ", \"parentId\": 123}"))
                    .andExpect(status()
                            .isCreated());
            ChildParentIT.idCount++;
        }

        @Test
        void findAll_shouldReturn401WhenNoTokenSent() throws Exception {
            mockMvc.perform(get("/childParents")
                    .accept("application/json"))
                    .andExpect(status()
                            .isUnauthorized());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        void findAll_shouldThrowErrorIfInvalidRole() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/childParents")
                    .accept("application/json"))
                    .andExpect(status()
                            .isForbidden());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void findAll_shouldReturnOkayIfParent() throws Exception {
            mockMvc.perform(get("/childParents")
                    .accept("application/json"))
                    .andExpect(status()
                            .isOk());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        void findById_shouldThrowErrorIfInvalidRole() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/childParents/1")
                    .accept("application/json"))
                    .andExpect(status()
                            .isForbidden());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void findById_shouldReturnOkayIfParent() throws Exception {
            mockMvc.perform(get("/childParents/" + ChildParentIT.idCount)
                    .accept("application/json"))
                    .andExpect(status()
                            .isOk());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        void save_shouldThrowErrorIfInvalidRole() throws Exception {
            // Act / Assert
            mockMvc.perform(post("/childParents")
                    .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{}"))
                    .andExpect(status()
                            .isForbidden());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void save_shouldReturnOkayIfParent() throws Exception {
            // Act / Assert
            mockMvc.perform(post("/childParents")
                    .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{}"))
                    .andExpect(status()
                            .isCreated());
        }

        @Test
        @WithMockUser(roles = {"PARENT", "CHILD"})
        void deleteById_shouldThrowErrorForEveryone() throws Exception {
            // Act / Assert
            mockMvc.perform(delete("/childParents/" + ChildParentIT.idCount)
                    .accept("application/json"))
                    .andExpect(status()
                            .isForbidden());
        }
    }

    @Nested
    @DisplayName("Status Codes")
    @AutoConfigureMockMvc
    @SpringBootTest
    class StatusCodes {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper mapper;

        @Test
        @DisplayName("Should return 409 Conflict, when duplicate Child created")
        @WithMockUser(roles = "PARENT")
        void save_shouldReturn_409Conflict_whenDuplicateEntityCreated() throws Exception {
            // Arrange
            final long duplicatedChildId = 123L;
            final String child = "{\"childId\": \"" + duplicatedChildId + "\", \"parentId\": 123}";
            mockMvc.perform(post("/childParents")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(child)
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isCreated());

            // Act / Assert
            final String resultAsString = mockMvc.perform(post("/childParents")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(child)
                    .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status()
                            .isConflict())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            assertThat("Expected response object", resultAsString, not(isEmptyString()));
            final Map<String, Object> resultAsMap =
                    mapper.readValue(resultAsString, new TypeReference<Map<String, Object>>() {
                    });
            assertThat("Expected 'cause' field to not exist", resultAsMap.get("cause"), nullValue());
            assertThat("Expected 'message' field to exist", resultAsMap.get("message"), notNullValue());
            final String expectedMessage = "Child already has a parent";
            assertThat("Expected 'message' field to have correct message",
                    resultAsMap.get("message"), equalTo(expectedMessage));

        }

    }
}
