package com.magipixel.bountyboard.childparent.model;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Tag("unit")
class ChildParentRepositoryTest {

    @Autowired
    private ChildParentRepository repository;

    @Test
    @WithMockUser(roles = "PARENT")
    void findByParentId() {
        // Arrange
        //    Add two ChildParent entities to search for
        final ChildParent childPrentA = new ChildParent();
        childPrentA.setChildId(1L);
        final long parentId = 123L;
        childPrentA.setParentId(parentId);
        final Long idA = repository.save(childPrentA).getId();
        final ChildParent childParentB = new ChildParent();
        childParentB.setChildId(2L);
        childParentB.setParentId(parentId);
        final Long idB = repository.save(childParentB).getId();

        //    Add distraction ChildParent
        final ChildParent childParentC = new ChildParent();
        childParentC.setChildId(3L);
        childParentC.setParentId(parentId + 99);
        repository.save(childParentC);

        // Act
        final List<ChildParent> result = repository.findByParentId(parentId);

        // Assert
        assertThat("Repository returned null", result, notNullValue());
        assertThat("Repository returned empty list", result, hasSize(greaterThan(0)));
        assertThat("Repository returned list with only one element", result, hasSize(greaterThan(1)));
        assertThat("Repository returned list with more than two elements", result, hasSize(2));
        childPrentA.setId(idA);
        childParentB.setId(idB);
        assertThat("Repository returned list with more than two elements", result, hasItems(childPrentA, childParentB));

    }

}