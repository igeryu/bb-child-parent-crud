package com.magipixel.bountyboard.childparent.config;

import com.magipixel.bountyboard.childparent.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
class CustomExceptionHandler {

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> duplicateEntity() {
        log.warn("Duplicate Child-Parent creation attempted");
        final ErrorResponse errorResponse = new ErrorResponse("Child already has a parent");
        return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
    }
}

