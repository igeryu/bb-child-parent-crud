package com.magipixel.bountyboard.childparent.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Optional;

public interface ChildParentRepository extends CrudRepository<ChildParent, Long> {
    @PreAuthorize("hasRole('ADMIN')")
    @Override
    void deleteById(final Long id);

    @PreAuthorize("hasRole('PARENT')")
    @Override
    Iterable<ChildParent> findAll();

    @PreAuthorize("hasRole('PARENT')")
    @Override
    Optional<ChildParent> findById(final Long id);

    List<ChildParent> findByParentId(final Long parentId);

    @PreAuthorize("hasRole('PARENT')")
    @Override
    <S extends ChildParent> S save(final S childParent);
}